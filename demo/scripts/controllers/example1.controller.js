﻿(function () {
    'use strict';

    angular.module('funcef-demo.controller')
        .controller('Example1Controller', Example1Controller);

    Example1Controller.$inject = [];

    /* @ngInject */
    function Example1Controller() {
        var vm = this;

        vm.number = 5;
    }
}());