﻿(function () {
    'use strict';

    angular
      .module('funcef-inputnumber.directive')
      .directive('ngfInputNumber', ngfInputNumber);

    ngfInputNumber.$inject = ['$timeout'];

    /* @ngInject */
    function ngfInputNumber($timeout) {
        return {
            restrict: 'EA',
            require: 'ngModel',
            templateUrl: 'views/input-number.view.html',
            scope: {
                model: '=ngModel',
                changeMethod: '&onChange',
                minlength: '=minLength',
                maxlength: '=maxLength',
                min: '=minValue',
                max: '=maxValue',
                placehoulder: '='
            },
            link: function (scope, elem, attrs, ctrl) {
                var timeoutFunction;
                var delay = 500;
                scope.$watch('model', function (newValue, oldValue) {
                    if (newValue && isNaN(newValue)) {
                        scope.model = parseInt(newValue.toString().replace(/[^0-9-]/g, ''));
                        return false;
                    }
                    
                    if (!isNaN(newValue) && !isNaN(oldValue)) {
                        $timeout.cancel(timeoutFunction);
                        timeoutFunction = $timeout(function () {
                            if (!angular.equals(newValue, oldValue) && scope.changeMethod) {
                                scope.changeMethod();
                            }
                        }, delay);
                    }
                });
            }
        };
    }
})();
