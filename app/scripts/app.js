﻿(function () {
    'use strict';
    /**
    * @ngdoc overview
    * @name inputnumber
    * @version 1.0.0
    * @inputnumber
    */

    angular.module('funcef-inputnumber.directive', []);

    angular
    .module('funcef-inputnumber', [
      'funcef-inputnumber.directive'
    ]);
})();