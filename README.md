# Componente - FUNCEF-InputNumber

## Conteúdos

1. [Descrição](#descrição)
2. [Instalação](#instalação)
3. [Script](#script)
4. [CSS](#css)
5. [Módulo](#módulo)
6. [Uso](#uso)
7. [Desinstalação](#desinstalação)

## Descrição

- Componente Input Number é um campo numérico com controles para aumemtar ou diminuir o valor.

 
## Instalação:

### Bower

- Necessário executar o bower install e passar o nome do componente.

```
bower install funcef-input-number --save.
```

## Script

```html
<script src="bower_components/funcef-inputnumber/dist/funcef-inputnumber.js"></script>
```

__ATENÇÃO:__ Esta linha já é adicionada automaticamente com o uso do grunt pelo wiredep

## CSS  

```html
<link rel="stylesheet" href="bower_components/funcef-inputnumber/dist/funcef-inputnumber.css">
```

__ATENÇÃO:__ Esta linha já é adicionada automaticamente com o uso do grunt pelo wiredep

## Modulo

- Adicione funfef-input-number dentro do modulo do Sistema.

```js
    angular
        .module('funcef-demo', ['funcef-inputnumber']);
```

## Uso

```html
<ngf-input-number ng-model="vm.number" min="1" max="20"></ngf-input-number>
```

### Parâmetros adicionais:

- ng-model (Obrigatório);
- change-method (Opcional);
- minlength (Opcional);
- maxlength (Opcional);
- min (Opcional);
- max (Opcional);
- placeholder (Opcional);


## Desinstalação:

```
bower uninstall funcef-input-number --save
```