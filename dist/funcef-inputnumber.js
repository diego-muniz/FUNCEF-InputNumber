(function () {
    'use strict';
    /**
    * @ngdoc overview
    * @name inputnumber
    * @version 1.0.0
    * @inputnumber
    */

    angular.module('funcef-inputnumber.directive', []);

    angular
    .module('funcef-inputnumber', [
      'funcef-inputnumber.directive'
    ]);
})();;;(function () {
    'use strict';

    angular
      .module('funcef-inputnumber.directive')
      .directive('ngfInputNumber', ngfInputNumber);

    ngfInputNumber.$inject = ['$timeout'];

    /* @ngInject */
    function ngfInputNumber($timeout) {
        return {
            restrict: 'EA',
            require: 'ngModel',
            templateUrl: 'views/input-number.view.html',
            scope: {
                model: '=ngModel',
                changeMethod: '&onChange',
                minlength: '=minLength',
                maxlength: '=maxLength',
                min: '=minValue',
                max: '=maxValue',
                placehoulder: '='
            },
            link: function (scope, elem, attrs, ctrl) {
                var timeoutFunction;
                var delay = 500;
                scope.$watch('model', function (newValue, oldValue) {
                    if (newValue && isNaN(newValue)) {
                        scope.model = parseInt(newValue.toString().replace(/[^0-9-]/g, ''));
                        return false;
                    }
                    
                    if (!isNaN(newValue) && !isNaN(oldValue)) {
                        $timeout.cancel(timeoutFunction);
                        timeoutFunction = $timeout(function () {
                            if (!angular.equals(newValue, oldValue) && scope.changeMethod) {
                                scope.changeMethod();
                            }
                        }, delay);
                    }
                });
            }
        };
    }
})();
;angular.module('funcef-inputnumber').run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('views/input-number.view.html',
    "<div class=\"input-group input-number\"> <span class=\"input-group-btn value-control\"> <button class=\"btn btn-default pr-5 pl-5\" ng-click=\"model = model - 1\"> <i class=\"fa fa-minus\"></i> </button> </span> <input type=\"text\" min=\"{{min}}\" max=\"{{max}}\" minlength=\"{{minlength}}\" maxlength=\"{{maxlength}}\" class=\"form-control text-center\" ng-model=\"model\"> <span class=\"input-group-btn value-control\"> <button class=\"btn btn-default pr-5 pl-5\" ng-click=\"model = model + 1\"> <i class=\"fa fa-plus\"></i> </button> </span> </div>"
  );

}]);
