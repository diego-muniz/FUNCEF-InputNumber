angular.module('funcef-inputnumber').run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('views/input-number.view.html',
    "<div class=\"input-group input-number\"> <span class=\"input-group-btn value-control\"> <button class=\"btn btn-default pr-5 pl-5\" ng-click=\"model = model - 1\"> <i class=\"fa fa-minus\"></i> </button> </span> <input type=\"text\" min=\"{{min}}\" max=\"{{max}}\" minlength=\"{{minlength}}\" maxlength=\"{{maxlength}}\" class=\"form-control text-center\" ng-model=\"model\"> <span class=\"input-group-btn value-control\"> <button class=\"btn btn-default pr-5 pl-5\" ng-click=\"model = model + 1\"> <i class=\"fa fa-plus\"></i> </button> </span> </div>"
  );

}]);
